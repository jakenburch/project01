<?php
session_start();

$answers = json_decode(file_get_contents("answer_key.json"), true);

function shuffle_assoc($answers) { 
  if (!is_array($answers)) return $answers; 

  $keys = array_keys($answers); 
  shuffle($keys); 
  $random = array(); 
  foreach ($keys as $key) { 
    $random[$key] = $answers[$key]; 
  }
  return $random; 
} 

$answers = shuffle_assoc($answers);

if(is_null($_SESSION['set'])){
    $_SESSION['answers'] = $answers;
    $_SESSION['counter'] = 1; 
    $_SESSION['set'] = 1;
}

$plate_number = key($_SESSION['answers']);

$question_number = $_SESSION['counter'];

$_SESSION['responses'] = [];

if(isset($_POST['response'])){
array_push($_SESSION['responses'], $_POST['response']); 
    $_SESSION['counter']++;
}  
?><!DOCTYPE html>
<html>
    <head>
    
    </head>
    <body>
        <h4>Viewing plate number </h4><span id="plate-number"><?= $plate_number ?></span></h4>
        <h4>Viewing question number <span id="question-number"><?=$question_number?></span> out of 25</h4> 
    
        <img src="test-images/<?=$plate_number?>.png" />
    
        <form method="POST" action="index.php">
            <input type="text" name="response"/>
            <input type="submit" value="Next" name="submit"/>
        </form>

    <a href="reset.php">reset</a>
    <?php print_r($_SESSION['answers']);
          echo $_POST['response'];
 ?>
    ____________________________________
    <?php print_r($_SESSION['responses']);
 ?>
    </body>
</html>