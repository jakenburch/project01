<?php

?><!DOCTYPE html>
<html>
    <head>
    
    </head>
    <body>
        <h1>Results Overview</h1>
        <table>
            <tr>
                <th>Vision type</th>
                <th>tally</th>
            </tr>
            <tr>
                <td>normal vision</td>
                <td>##</td>
            </tr>
            <tr>
                <td>red green deficiency</td>
                <td>##</td>
            </tr>
            <tr>
                <td>protanopia or protanomaly</td>
                <td>##</td>
            </tr>
            <tr>
                <td>deuteranopia or deuteranomaly</td>
                <td>##</td>
            </tr>
            <tr>
                <td>no indication</td>
                <td>##</td>
            </tr>
        </table>
    </body>
</html>